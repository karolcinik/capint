package org.cap.learning.ex_spring.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.cap.learning.ex_spring.common.Employee;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EmployeeRestControllerTest {

    private MockMvc mockMvc;
    private MockHttpSession session;

    @Autowired
    private WebApplicationContext context;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .build();
    }
    
    
    @Test
    public void shouldReturnListOf10Employees() throws Exception {
        // given
        Gson jsonMapper = new Gson();
        Type listType = new TypeToken<ArrayList<Employee>>(){}.getType();

        // when
        ResultActions result = mockMvc.perform(get(UriConf.EMPLOYEE_API_URI)
                .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk());

        List<Employee> employeeList = jsonMapper.fromJson(result.andReturn().getResponse().getContentAsString(), listType);

        // than
        Assert.assertEquals(10, employeeList.size());
    }
    
    @Test
    public void shouldReturnOlegPutinForId10001() throws Exception {
        // given
        Gson jsonMapper = new Gson();
        Long userId = 10001L;
        String uri = String.join("/",UriConf.EMPLOYEE_API_URI, String.valueOf(userId));

        // when
        ResultActions result = mockMvc.perform(get(uri)
                .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk());

        Employee employee = jsonMapper.fromJson(result.andReturn().getResponse().getContentAsString(), Employee.class);

        // than
        Assert.assertEquals("Oleg", employee.getFirstName());
        Assert.assertEquals("Putin", employee.getLastName());
        Assert.assertEquals(10001L, (long)employee.getEmployeeNumber());
        
    }
}