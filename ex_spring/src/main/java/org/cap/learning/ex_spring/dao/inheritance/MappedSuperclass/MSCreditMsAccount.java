package org.cap.learning.ex_spring.dao.inheritance.MappedSuperclass;

import java.math.BigDecimal;

import javax.persistence.Entity;

@Entity(name = "ms_credit_account")
public class MSCreditMsAccount extends MsAccount {

    private BigDecimal creditLimit;

    public BigDecimal getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(BigDecimal creditLimit) {
        this.creditLimit = creditLimit;
    }
}
