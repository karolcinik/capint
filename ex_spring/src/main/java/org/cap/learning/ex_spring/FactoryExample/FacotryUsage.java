package org.cap.learning.ex_spring.FactoryExample;

public class FacotryUsage {
    
    public FacotryUsage() {
        
        
        CarFactory carFactory = new CarFactory();
        
        Audi audi = (Audi) carFactory.getMeSomeCar(CarType.AUDI);
    }
}
