package org.cap.learning.ex_spring.dao.inheritance.SingleTable;

import java.math.BigDecimal;

import javax.persistence.Entity;

@Entity(name = "st_credit_account")
public class StCreditAccount extends StAccount {

    private BigDecimal creditLimit;

    public BigDecimal getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(BigDecimal creditLimit) {
        this.creditLimit = creditLimit;
    }
}