package org.cap.learning.ex_spring.service;

import java.util.ArrayList;
import java.util.List;

import org.cap.learning.ex_spring.beans.CapService;
import org.cap.learning.ex_spring.common.Employee;
import org.cap.learning.ex_spring.common.mappers.EmployeeMapper;
import org.cap.learning.ex_spring.common.mappers.NameOfEmployee;
import org.cap.learning.ex_spring.dao.entity.EmployeeEntity;
import org.cap.learning.ex_spring.dao.repositories.EmployeesRepository;
import org.cap.learning.ex_spring.dao.repositories.SalariesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeesService {
    
    @Autowired
    CapService capService;
    
    @Autowired
    EmployeesRepository employeesRepository;

    @Autowired
    SalariesRepository salariesRepository;
    
    public List<Employee> getEmployees() {
        final List<Employee> employees = new ArrayList<>();
        employeesRepository.findAll()
                .forEach(e -> employees.add(EmployeeMapper.toEmployee(e)));
        return  employees;
    }

    public Employee getEmployeeById(Long id) {
        Employee employee = EmployeeMapper.toEmployee(employeesRepository.getEmployerByEmpNo(id));
        capService.calculatePayroll(employee);
        return employee;
    }
    public List<NameOfEmployee> getAllNames() {
        List<NameOfEmployee> nameOfEmployeeList = new ArrayList<>();
        List<EmployeeEntity> employeeEntityList = employeesRepository.findAll();

        for (EmployeeEntity employeeEntity : employeeEntityList) {
            NameOfEmployee nameOfEmployee = new NameOfEmployee();
            nameOfEmployee.setEmployeeName(employeeEntity.getFirstName());
            nameOfEmployee.setEmployeeSurname(employeeEntity.getLastName());

            nameOfEmployeeList.add(nameOfEmployee);
        }
        return nameOfEmployeeList;
    }

   }
