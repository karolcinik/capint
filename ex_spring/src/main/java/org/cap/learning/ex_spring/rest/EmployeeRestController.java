package org.cap.learning.ex_spring.rest;

import java.util.List;

import org.cap.learning.ex_spring.common.Employee;
import org.cap.learning.ex_spring.common.mappers.NameOfEmployee;
import org.cap.learning.ex_spring.service.EmployeesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeRestController {

    @Autowired
    EmployeesService employeesService;

    @RequestMapping(value = "api/employees", method = RequestMethod.GET)
    public List<Employee> getAllEmployees() {
        return employeesService.getEmployees();
    }

    @RequestMapping(value = "api/employees/{id}", method = RequestMethod.GET)
    public Employee getAllEmployees(@PathVariable("id") Long id) {
        return employeesService.getEmployeeById(id);
    }

    ////http://localhost:9000/api/names
    @RequestMapping (value = "api/names", method = RequestMethod.GET)
    public List<NameOfEmployee> getNamesOfAllEmployees() {return employeesService.getAllNames();}
}
