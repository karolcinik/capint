package org.cap.learning.ex_spring.dao.repositories;


import org.cap.learning.ex_spring.dao.entity.EmployeeEntity;
import org.cap.learning.ex_spring.dao.entity.SalaryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SalariesRepository extends JpaRepository<SalaryEntity, Long> {

    List<SalaryEntity> getSalaryEntityByEmployeeEntity(EmployeeEntity employeeNumber);
    List<SalaryEntity> getSalaryEntitiesByEmployeeEntityEmpNo(Long empNo);
}
