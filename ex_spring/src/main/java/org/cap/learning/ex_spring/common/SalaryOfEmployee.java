package org.cap.learning.ex_spring.common;

/**
 * Created by MAADAMEK on 29.08.2017.
 */
public class SalaryOfEmployee {

    private String employeeName;
    private String empoyeeSurname;
    private Long employeeSalary;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmpoyeeSurname() {
        return empoyeeSurname;
    }

    public void setEmpoyeeSurname(String empoyeeSurname) {
        this.empoyeeSurname = empoyeeSurname;
    }

    public Long getEmployeeSalary() {
        return employeeSalary;
    }

    public void setEmployeeSalary(Long employeeSalary) {
        this.employeeSalary = employeeSalary;
    }
}
