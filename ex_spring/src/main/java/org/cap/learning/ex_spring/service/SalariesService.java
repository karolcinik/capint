package org.cap.learning.ex_spring.service;

import org.cap.learning.ex_spring.common.SalaryOfEmployee;
import org.cap.learning.ex_spring.common.SalaryOfOneEmployee;
import org.cap.learning.ex_spring.dao.entity.EmployeeEntity;
import org.cap.learning.ex_spring.dao.entity.SalaryEntity;
import org.cap.learning.ex_spring.dao.repositories.EmployeesRepository;
import org.cap.learning.ex_spring.dao.repositories.SalariesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by UFORENC on 18.08.2017.
 */

@Service
public class SalariesService {

    @Autowired
    SalariesRepository salariesRepository;

    @Autowired
    EmployeesRepository employeesRepository;

    public List<SalaryEntity> getSalariesForEmployeeEmpNo(long empNo) {
        return salariesRepository.getSalaryEntitiesByEmployeeEntityEmpNo(empNo);

    }

    public List<SalaryOfEmployee> getAllSalaries() {
        List<SalaryOfEmployee> salaryOfEmployeeList = new ArrayList<>();

        List<EmployeeEntity> employeeEntityList = employeesRepository.findAll();
        for (EmployeeEntity employeeEntity : employeeEntityList) {
            List<SalaryEntity> salaryEntityList = getSalariesForEmployeeEmpNo(employeeEntity.getEmpNo());
            Long salary = 0L;
            for (SalaryEntity salaryEntity : salaryEntityList) {
                salary = (salary + salaryEntity.getSalary());

            }
            SalaryOfEmployee salaryOfEmployee = new SalaryOfEmployee();
            salaryOfEmployee.setEmployeeName(employeeEntity.getFirstName());
            salaryOfEmployee.setEmpoyeeSurname(employeeEntity.getLastName());
            salaryOfEmployee.setEmployeeSalary(salary);
            salaryOfEmployeeList.add(salaryOfEmployee);
        }

        return salaryOfEmployeeList;
    }

    public List<SalaryOfOneEmployee> getOneSalary(Long empNo) {
        return null;
    }
}



