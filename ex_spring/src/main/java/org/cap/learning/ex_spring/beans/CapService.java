package org.cap.learning.ex_spring.beans;


import org.cap.learning.ex_spring.common.Employee;

public interface CapService {
    
    CapService execute(Object object);
    Object calculatePayroll(Employee e);
}
