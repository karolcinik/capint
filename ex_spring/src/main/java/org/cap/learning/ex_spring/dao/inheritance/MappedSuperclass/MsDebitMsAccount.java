package org.cap.learning.ex_spring.dao.inheritance.MappedSuperclass;

import java.math.BigDecimal;

import javax.persistence.Entity;

@Entity(name = "ms_debit_account")
public class MsDebitMsAccount extends MsAccount {

    private BigDecimal overdraftFee;

    public BigDecimal getOverdraftFee() {
        return overdraftFee;
    }

    public void setOverdraftFee(BigDecimal overdraftFee) {
        this.overdraftFee = overdraftFee;
    }
}