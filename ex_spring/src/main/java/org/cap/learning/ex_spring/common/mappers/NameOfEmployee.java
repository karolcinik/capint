package org.cap.learning.ex_spring.common.mappers;

import org.cap.learning.ex_spring.dao.entity.EmployeeEntity;

import java.util.List;

/**
 * Created by MAADAMEK on 01.09.2017.
 */
public class NameOfEmployee {

    private String employeeName;
    private String employeeSurname;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeSurname() {
        return employeeSurname;
    }

    public void setEmployeeSurname(String employeeSurname) {
        this.employeeSurname = employeeSurname;
    }
}
