package org.cap.learning.ex_spring.rest;

import org.cap.learning.ex_spring.common.Salary;
import org.cap.learning.ex_spring.common.SalaryOfEmployee;
import org.cap.learning.ex_spring.common.mappers.SalaryMapper;
import org.cap.learning.ex_spring.common.SalaryOfOneEmployee;
import org.cap.learning.ex_spring.service.SalariesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by UFORENC on 18.08.2017.
 */

@RestController//informuje springa
public class SalariesRestController {
    @Autowired
    SalariesService salariesService;

    @RequestMapping(value = "api/salaries/{empNo}", method = RequestMethod.GET)
    public List<Salary> getAllEmployees(@PathVariable("empNo") Long empNo) {
        return SalaryMapper.toListSalary(salariesService.getSalariesForEmployeeEmpNo(empNo));
    }

    //http://localhost:8080/api/salaries
    @RequestMapping(value = "api/salaries", method = RequestMethod.GET)
    public List<SalaryOfEmployee> getSalariesOfAllEmployees() {
        return salariesService.getAllSalaries();
    }

    //http://localhost:9000/api/salary/id
    @RequestMapping(value = "api/salary/{empNo}", method = RequestMethod.GET)
    public List<SalaryOfOneEmployee> getSalaryOfOneEmployee(@PathVariable("empNo") Long empNo) {
        return salariesService.getOneSalary(empNo);
    }
}
