package org.cap.learning.ex_spring.common.dao;


import java.util.Date;

import org.cap.learning.ex_spring.common.Gender;

public interface EmployeeDao {

    Long getEmpNo();
    Date getBirthDate();
    String getFirstName();
    String getLastName();
    Gender getGender();
    Date getHireDate();
}
