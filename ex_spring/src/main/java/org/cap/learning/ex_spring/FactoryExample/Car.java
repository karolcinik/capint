package org.cap.learning.ex_spring.FactoryExample;

public interface Car {
    
    Car getCar();
}
