package org.cap.learning.ex_spring.dao.entity;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "salaries")
public class SalaryEntity {
    
    @EmbeddedId
    SalariesId salariesId;
    
    @Column(name = "salary")
    Long Salary;
    
    @Column(name = "to_date")
    Date toDate;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "emp_no",
            insertable = false, updatable = false,
            foreignKey = @ForeignKey(name = "EMPLOYER_EMP_NO_FK")
    )
    private EmployeeEntity employeeEntity;

    public SalariesId getSalariesId() {
        return salariesId;
    }

    public void setSalariesId(SalariesId salariesId) {
        this.salariesId = salariesId;
    }

    public Long getSalary() {
        return Salary;
    }

    public void setSalary(Long salary) {
        Salary = salary;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public EmployeeEntity getEmployeeEntity() {
        return employeeEntity;
    }

    public void setEmployeeEntity(EmployeeEntity employeeEntity) {
        this.employeeEntity = employeeEntity;
    }
}

