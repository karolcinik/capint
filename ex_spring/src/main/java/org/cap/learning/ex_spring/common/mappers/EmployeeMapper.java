package org.cap.learning.ex_spring.common.mappers;


import org.cap.learning.ex_spring.common.Employee;
import org.cap.learning.ex_spring.common.dao.EmployeeDao;
import org.joda.time.DateTime;

public class EmployeeMapper {
    
    public static Employee toEmployee(EmployeeDao employeeDao) {
        return new Employee(
                employeeDao.getEmpNo(),
                new DateTime(employeeDao.getBirthDate()),
                employeeDao.getFirstName(),
                employeeDao.getLastName(),
                employeeDao.getGender(),
                new DateTime(employeeDao.getHireDate())
        );
    }
    
}
