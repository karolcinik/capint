package org.cap.learning.ex_spring.beans;

import org.cap.learning.ex_spring.common.Employee;

public class DefaultCapService implements CapService {
    
    @Override
    public CapService execute(Object object) {
        return new DefaultCapService();
    }

    @Override
    public Object calculatePayroll(Employee e) {
        return new Object();
    }

}
