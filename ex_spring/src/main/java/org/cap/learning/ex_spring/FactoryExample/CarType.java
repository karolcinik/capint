package org.cap.learning.ex_spring.FactoryExample;

public enum CarType {
    BMW, AUDI
}
