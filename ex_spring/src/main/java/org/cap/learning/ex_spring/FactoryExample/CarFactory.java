package org.cap.learning.ex_spring.FactoryExample;

import static org.cap.learning.ex_spring.FactoryExample.CarType.AUDI;

public class CarFactory {
    
    Car getMeSomeCar(CarType carType) {
        switch(carType) {
            case AUDI: new Audi();
            case BMW: new Bmw();
        }
            return null;
    }
    
}
