package org.cap.learning.ex_spring.common.mappers;

import org.cap.learning.ex_spring.common.Salary;
import org.cap.learning.ex_spring.dao.entity.SalaryEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by UFORENC on 18.08.2017.
 */
public class SalaryMapper {
    public static Salary toSalary(SalaryEntity salaryEntity){
        return new Salary(
                salaryEntity.getSalary(),
                salaryEntity.getToDate(),
                salaryEntity.getSalariesId().getEmpNo(),
                salaryEntity.getSalariesId().getFromDate()
        );
    }

    public static List<Salary> toListSalary(List<SalaryEntity> entityList) {
        List<Salary> salaryList = new ArrayList<>();
        for(SalaryEntity salaryEntity : entityList){
            salaryList.add(SalaryMapper.toSalary(salaryEntity));
        }
        return salaryList;
    }
}
