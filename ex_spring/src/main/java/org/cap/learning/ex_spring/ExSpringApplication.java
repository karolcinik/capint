package org.cap.learning.ex_spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@ComponentScan(basePackages = {
//        "org.cap.learning.ex_spring", 
//        "org.cap.learning.ex_spring.rest",
//        "org.cap.learning.ex_spring.service",
//        "org.cap.learning.ex_spring.dao",
//        "org.cap.learning.ex_spring.common",
//})
public class ExSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExSpringApplication.class, args);
        
    }
}
