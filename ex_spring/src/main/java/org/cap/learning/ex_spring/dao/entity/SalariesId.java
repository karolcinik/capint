package org.cap.learning.ex_spring.dao.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Date;

@Embeddable
public class SalariesId implements Serializable {

    @Column(name = "emp_no")
    Long empNo;

    @Column(name = "fromDate")
    Date fromDate;

    public SalariesId(){

    }


    public SalariesId(Long empNo, Date fromDate) {
        this.empNo = empNo;
        this.fromDate = fromDate;
    }

    public Long getEmpNo() {
        return empNo;
    }

    public void setEmpNo(Long empNo) {
        this.empNo = empNo;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }


}
