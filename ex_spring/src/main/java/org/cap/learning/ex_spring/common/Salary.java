package org.cap.learning.ex_spring.common;

import java.util.Date;

/**
 * Created by UFORENC on 18.08.2017.
 */
public class Salary {

    private Long Salary;
    private Date toDate;
    private Long empNo;
    private Date fromDate;

    public Salary(Long salary, Date toDate, Long empNo, Date fromDate) {
        Salary = salary;
        this.toDate = toDate;
        this.empNo = empNo;
        this.fromDate = fromDate;
    }

    public Long getSalary() {
        return Salary;
    }

    public void setSalary(Long salary) {
        Salary = salary;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public Long getEmpNo() {
        return empNo;
    }

    public void setEmpNo(Long empNo) {
        this.empNo = empNo;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Salary salary = (Salary) o;

        if (Salary != null ? !Salary.equals(salary.Salary) : salary.Salary != null) return false;
        if (toDate != null ? !toDate.equals(salary.toDate) : salary.toDate != null) return false;
        if (empNo != null ? !empNo.equals(salary.empNo) : salary.empNo != null) return false;
        return fromDate != null ? fromDate.equals(salary.fromDate) : salary.fromDate == null;
    }

    @Override
    public int hashCode() {
        int result = Salary != null ? Salary.hashCode() : 0;
        result = 31 * result + (toDate != null ? toDate.hashCode() : 0);
        result = 31 * result + (empNo != null ? empNo.hashCode() : 0);
        result = 31 * result + (fromDate != null ? fromDate.hashCode() : 0);
        return result;
    }
}
