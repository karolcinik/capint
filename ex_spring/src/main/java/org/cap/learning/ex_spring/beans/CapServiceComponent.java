package org.cap.learning.ex_spring.beans;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class CapServiceComponent {
    
    @Bean
    @Scope("prototype")
    public CapService getService() {
        return new DefaultCapService();
    }
    
    
}

