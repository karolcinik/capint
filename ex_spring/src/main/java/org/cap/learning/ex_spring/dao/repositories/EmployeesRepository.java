package org.cap.learning.ex_spring.dao.repositories;


import org.cap.learning.ex_spring.dao.entity.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface EmployeesRepository extends JpaRepository<EmployeeEntity, Long> {

    EmployeeEntity getEmployerByEmpNo(Long empNo);

    EmployeeEntity deleteByEmpNo(Long id);
}
