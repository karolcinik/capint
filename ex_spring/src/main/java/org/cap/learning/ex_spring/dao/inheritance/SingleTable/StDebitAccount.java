package org.cap.learning.ex_spring.dao.inheritance.SingleTable;

import java.math.BigDecimal;

import javax.persistence.Entity;

@Entity(name = "st_debit_account")
public class StDebitAccount extends StAccount {

    private BigDecimal overdraftFee;

    public BigDecimal getOverdraftFee() {
        return overdraftFee;
    }

    public void setOverdraftFee(BigDecimal overdraftFee) {
        this.overdraftFee = overdraftFee;
    }
}
