package org.cap.learning.ex2DataLoading;

import java.util.*;

/**
 * Created by MAADAMEK on 08.08.2017.
 */
public class CollectonsExample {

    public void someSimpleList(){
        List<String> simpleList = new LinkedList<>();

        simpleList.add("String1");
        simpleList.add("String2");
        simpleList.add("String3");

//        for(String item : simpleList){
//            System.out.println(item);
//        }

//        Iterator iterator  = simpleList.iterator();
//        System.out.println(iterator.next());
//        System.out.println(iterator.next());
//        System.out.println(iterator.next());

//    while(iterator.hasNext()){
//        System.out.println(iterator.next());
//    }

        Set<String> simpleSet = new HashSet<>();
        simpleSet.add("String1");
        simpleSet.add("String4");
        simpleSet.add("String8");
        simpleSet.add("String7");
        simpleSet.add("String3");
        simpleSet.add("String9");

        for(String item : simpleSet){
            System.out.println(item);
        }

        Map<String, String> simpleMap = new HashMap();

        simpleMap.put("klucz1", "wartosc1");
        simpleMap.put("klucz2", "wartosc2");
        simpleMap.put("klucz3", "wartosc3");

        System.out.print(simpleMap.get("klucz1"));

    }
}
