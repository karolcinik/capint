package org.cap.learning.ex2DataLoading.entitiy;


import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class EmployeesSalaries {

    private String employerNumber;
    private Long salary;
    //    private String fromDate;
    private DateTime fromDate;
    private DateTime toDate;
    private DateTime birthDate;
    private String firstName;
    private String lastName;
    private String gender;
    private DateTime hireDate;

    public EmployeesSalaries(String[] parameters) {
        if (parameters.length == 9) {
            setEmployerNumber(parameters[0]);
            setSalary(Long.valueOf(parameters[1]));
            setFromDate(convertStringToDateTime(parameters[2]));
            setToDate(convertStringToDateTime(parameters[3]));
            setBirthDate(convertStringToDateTime(parameters[4]));
            setFirstName(parameters[5]);
            setLastName(parameters[6]);
            setGender(parameters[7]);
            setHireDate(convertStringToDateTime(parameters[8]));
        }
    }

    public EmployeesSalaries() {
    }

    public DateTime convertStringToDateTime(String date) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTime dateTime = formatter.parseDateTime(date);
        return dateTime;
    }

    public String getEmployerNumber() {
        return employerNumber;
    }

    public void setEmployerNumber(String employerNumber) {
        this.employerNumber = employerNumber;
    }

    public Long getSalary() {
        return salary;
    }

    public void setSalary(Long salary) {
        this.salary = salary;
    }


    public DateTime getFromDate() {

        return fromDate;
    }

    public void setFromDate(DateTime fromDate) {
        this.fromDate = fromDate;
    }


    public DateTime getToDate() {
        return toDate;
    }

    public void setToDate(DateTime toDate) {
        this.toDate = toDate;
    }

    public DateTime getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(DateTime birthDate) {
        this.birthDate = birthDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public DateTime getHireDate() {
        return hireDate;
    }

    public void setHireDate(DateTime hireDate) {
        this.hireDate = hireDate;
    }

    @Override
    public boolean equals(Object o) {

        EmployeesSalaries employeesSalaries = (EmployeesSalaries) o;

        if (this.getFirstName().equals(employeesSalaries.getFirstName())) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        int newCode = 19;
        newCode = 17 * newCode + employerNumber.hashCode();
        newCode = 19 * newCode + salary.hashCode();
        newCode = 17 * newCode + fromDate.hashCode();
        return newCode;
    }
}
