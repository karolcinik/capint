package org.cap.learning.ex2DataLoading;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.cap.learning.ex2DataLoading.entitiy.EmployeesSalaries;

public class CSVLoader {

    private static final String LYNC_ACCOUNT_MIGRATION_CSV = "LyncAccountMigration.csv";

    public static CSVLoader getInstance() {
        return new CSVLoader();
    }

    public List<EmployeesSalaries> employeesSalariesList() {

        File data = new File(LYNC_ACCOUNT_MIGRATION_CSV);

        try (Stream<String> stream = Files.lines(Paths.get(data.getAbsolutePath()))) {

            return stream.map(s -> mapStringToEmployeesSalaries(s))
                    .collect(Collectors.toList());

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public long findMaxSalary() {
        List<EmployeesSalaries> employeesSalariesList = employeesSalariesList();
        long salary = 0;

        for (EmployeesSalaries employeesSalaries : employeesSalariesList) {
            if (employeesSalaries.getSalary() > salary) {
                salary = employeesSalaries.getSalary();
            }
        }
        return salary;
    }

    public long findMinSalary() {

        List<EmployeesSalaries> employeesSalariesList = employeesSalariesList();
        long salary = 0;

        boolean isFirstElement = true;
        for (EmployeesSalaries employeesSalaries : employeesSalariesList) {
            if (isFirstElement) {
                salary = employeesSalaries.getSalary();
                isFirstElement = false;
            } else {
                if (employeesSalaries.getSalary() < salary) {
                    salary = employeesSalaries.getSalary();
                }

            }
        }
        return salary;
    }
    public String findNameWithMaxSalary() {
        List<EmployeesSalaries> employeesSalariesList = employeesSalariesList();
        String name = null;
        long salary = 0;

        for (EmployeesSalaries employeesSalaries : employeesSalariesList) {
            if (employeesSalaries.getSalary() > salary ) {
                salary = employeesSalaries.getSalary();
                name = employeesSalaries.getFirstName();
            }
        }
        return name;
    }
    public String findNameWithMinSalary(){
        List<EmployeesSalaries> employeesSalariesList = employeesSalariesList();
        String name = null;
        long salary = 0;
        boolean isFirstElement = true;
        for (EmployeesSalaries employeesSalaries : employeesSalariesList) {
            if (isFirstElement) {
                salary = employeesSalaries.getSalary();
                isFirstElement = false;
            } else {
                if (employeesSalaries.getSalary() < salary) {
                    salary = employeesSalaries.getSalary();
                    name = employeesSalaries.getFirstName();
                }

            }
        }

        return name;
    }

    private EmployeesSalaries mapStringToEmployeesSalaries(String salaries) {
        return new EmployeesSalaries(salaries.split(","));
    }

}
