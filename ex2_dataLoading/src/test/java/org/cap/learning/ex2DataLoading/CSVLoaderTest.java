package org.cap.learning.ex2DataLoading;

import java.util.ArrayList;
import java.util.List;

import org.cap.learning.ex2DataLoading.entitiy.EmployeesSalaries;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Test;
import static java.lang.System.*;

public class CSVLoaderTest {

    static int x = 0;

    @Test
    public void shouldContain10000Elements() {
        
        int x = 9;
        
        System.out.println(x);
        
        
        CSVLoader csvLoader = CSVLoader.getInstance();

        List<EmployeesSalaries> employeesSalaries = csvLoader.employeesSalariesList();

        Assert.assertEquals(10000, employeesSalaries.size());
    }

    @Test
    public void shouldContainDateTimeForElements() {
        CSVLoader csvLoader = CSVLoader.getInstance();

        List<EmployeesSalaries> employeesSalaries = csvLoader.employeesSalariesList();

        EmployeesSalaries employeesSalaries1 = employeesSalaries.get(0);

        DateTime dateTime = employeesSalaries1.getFromDate();

        Assert.assertEquals(10000, employeesSalaries.size());
    }

    @Test
    public void shouldReturnCorrectDate() {

        String date = "1988-06-25";

        EmployeesSalaries employeesSalaries = new EmployeesSalaries();

        DateTime dateTime = employeesSalaries.convertStringToDateTime(date);

        Assert.assertEquals(dateTime.getYear(), 1988);
        Assert.assertEquals(dateTime.getMonthOfYear(), 06);
        Assert.assertEquals(dateTime.getDayOfMonth(), 25);

    }

    @Test
    public void shouldReturnBirthDateAsDateTime() {
        String[] arguments = {"10001", "62102", "1987-06-26", "1988-06-25", "1955-01-21", "Kyoichi", "Maliniak", "M", "1989-09-12"};

        EmployeesSalaries employeesSalaries = new EmployeesSalaries(arguments);
        DateTime dateTime = employeesSalaries.getBirthDate();
        Assert.assertEquals(dateTime.getYear(), 1955);
        Assert.assertEquals(dateTime.getMonthOfYear(), 1);
        Assert.assertEquals(dateTime.getDayOfMonth(), 21);
    }

    @Test
    public void shouldReturnToDateAsDateTime() {
        String[] arguments = {"10001", "62102", "1987-06-26", "1988-06-25", "1955-01-21", "Kyoichi", "Maliniak", "M", "1989-09-12"};
        EmployeesSalaries employeesSalaries = new EmployeesSalaries(arguments);
        DateTime dateTime = employeesSalaries.getToDate();
        Assert.assertEquals(dateTime.getYear(), 1988);
        Assert.assertEquals(dateTime.getMonthOfYear(), 6);
        Assert.assertEquals(dateTime.getDayOfMonth(), 25);
    }

    @Test
    public void shouldReturnHireDateAsDateTime() {
        String[] arguments = {"10001", "62102", "1987-06-26", "1988-06-25", "1955-01-21", "Kyoichi", "Maliniak", "M", "1989-09-12"};
        EmployeesSalaries employeesSalaries = new EmployeesSalaries(arguments);
        DateTime dateTime = employeesSalaries.getHireDate();
        Assert.assertEquals(dateTime.getYear(), 1989);
        Assert.assertEquals(dateTime.getMonthOfYear(), 9);
        Assert.assertEquals(dateTime.getDayOfMonth(), 12);
    }

    @Test
    public void shouldReturnMaxSalary() {
        CSVLoader loader = CSVLoader.getInstance();
        Assert.assertEquals(114969, loader.findMaxSalary());
    }

    @Test
    public void shouldReturnMinSalary() {
        CSVLoader loader = CSVLoader.getInstance();
        Assert.assertEquals(39275, loader.findMinSalary());
    }

    @Test
    public void shouldReturnNameWithMaxSalary() {
        CSVLoader loader = CSVLoader.getInstance();
        Assert.assertEquals("Laurentiu", loader.findNameWithMaxSalary());
    }

    @Test
    public void shouldReturnNameWithMinSalary() {
        CSVLoader loader = CSVLoader.getInstance();
        Assert.assertEquals("Monique", loader.findNameWithMinSalary());
    }

    @Test
    public void scrambling() {

        CSVLoader loader = CSVLoader.getInstance();

        WV wv1 = new WV();
        Audi audi = new Audi();
        BMW bmw = new BMW();
        Rower rower = new Rower();


        List<UkladKierowniczy> listaPozajdow = new ArrayList();


        for (UkladKierowniczy ukladKierowniczy : listaPozajdow) {
            ukladKierowniczy.skrecWLewo();
        }

        UkladKierowniczy ukladKierowniczy = wv1;
        Samochod samochod = wv1;

        if (samochod instanceof  WV) {
            WV wv = ((WV)samochod);
        }

    }

}

<<<<<<< HEAD

interface UkladKierowniczy {
    public void skrecWLewo();
}

class Samochod implements UkladKierowniczy {

    public int getIloscPaliwa() {
        return 0;
    }

    public String getKolor() {
        return "";
    }

    @Override
    public void skrecWLewo() {
        // skrecamy w lewo
    }
}

class WV extends Samochod {

    public String getSkandalWv() {
        return "";
    }

}

class Audi extends Samochod {

}

class BMW extends Samochod {

}


class Rower implements UkladKierowniczy {

    @Override
    public void skrecWLewo() {

    }

    public String getHipster() {
        return "";
    }
}
=======
class If{
    
    static {
        out.println();
        short k = 5;
        Double i = 0.0 + k;
        
       
    }
    boolean func (int y ) {
        int f;
        final int x;
        x = 10;
        
        if (y>10) return true; else return false;
    }
}
>>>>>>> 8791a85734bc5ff8b10fab342c02bb667b5ab6f6
