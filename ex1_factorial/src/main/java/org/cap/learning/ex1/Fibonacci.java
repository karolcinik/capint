package org.cap.learning.ex1;

public class Fibonacci {

    public long calculate(int fibonacci) {
        if (fibonacci < 0) {
            throw new RuntimeException("fibonacci should not be lower than 0");
        }
        if (fibonacci == 0 ) {
            return 0;
        }
        if (fibonacci == 1) {
            return 1;
        } else {
            return calculate(fibonacci -1) + calculate(fibonacci-2);
        }
    }
}
