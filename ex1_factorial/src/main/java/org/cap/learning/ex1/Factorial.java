package org.cap.learning.ex1;


public class Factorial {
    
    public long calculate(int factorial) {
        if (factorial < 0) {
            throw new RuntimeException("factorial should not be lower than 0");
        }
        if (factorial <= 1) {
            return 1;
        } else {
            return factorial * calculate(factorial -1);
        }
    }
}
