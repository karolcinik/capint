package org.cap.learning;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ListExample {


    public void example() {

        List<String> stringList = new ArrayList<String>();
        stringList.add("ALA");
        stringList.add("Beata");
        stringList.add("Celina");
        stringList.add("Danuta");


        Iterator<String> iterator = stringList.iterator();
//        String nextValue = null;
//        while(iterator.hasNext()) {
//            nextValue = iterator.next();
//        }
        
        // foreach
        for( String nextValue : stringList){
            
        }


        Set<String> stringSet =  new HashSet<String>();

        stringSet.add("ALA");
        stringSet.add("ALA");
        stringSet.add("Beata");
        stringSet.add("Celina");
        stringSet.add("Danuta");


        Map<String, String> stringMap = new HashMap<String, String>();
        
        stringMap.put("klucz", "wartosc");
        
        String wartosc = stringMap.get("klucz");
        
        
    }

}
