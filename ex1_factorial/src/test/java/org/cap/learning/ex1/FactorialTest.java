package org.cap.learning.ex1;

import org.junit.Assert;
import org.junit.Test;


public class FactorialTest {
    
    @Test
    public void calculate_shouldReturn1For0 (){
        
        // given
        Factorial factorial = new Factorial();
        
        // when
        long result = factorial.calculate(0);
        
        // then
        Assert.assertEquals(result, 1);
    }

    @Test
    public void calculate_shouldReturn1For1 (){

        // given
        Factorial factorial = new Factorial();

        // when
        long result = factorial.calculate(1);

        // then
        Assert.assertEquals(result, 1);
    }

    @Test
    public void calculate_shouldReturn2For2 (){

        // given
        Factorial factorial = new Factorial();

        // when
        long result = factorial.calculate(2);

        // then
        Assert.assertEquals(result, 2);
    }

    @Test
    public void calculate_shouldReturn6For3 (){

        // given
        Factorial factorial = new Factorial();

        // when
        long result = factorial.calculate(3);

        // then
        Assert.assertEquals(result, 6);
    }

    @Test
    public void calculate_shouldReturn120For5 (){

        // given
        Factorial factorial = new Factorial();

        // when
        long result = factorial.calculate(5);

        // then
        Assert.assertEquals(result, 120);
    }

    @Test(expected = Exception.class)
    public void calculate_shouldReturnExceptionForLessThan0 (){

        // given
        Factorial factorial = new Factorial();

        // when
        long result = factorial.calculate(-1);

        // then
    }

}