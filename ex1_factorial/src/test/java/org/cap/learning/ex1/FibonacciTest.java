package org.cap.learning.ex1;
import org.junit.Assert;
import org.junit.Test;


public class FibonacciTest {

    @Test
    public void calculate_ShouldReturn0For0(){
        // given
        Fibonacci fibonacci = new Fibonacci();

        // when
        long result = fibonacci.calculate(0);

        // then
        Assert.assertEquals(result, 0);
    }

    @Test
    public void calculate_ShouldReturn1For1(){
        // given
        Fibonacci fibonacci = new Fibonacci();

        // when
        long result = fibonacci.calculate(1);

        // then
        Assert.assertEquals(result, 1);
    }

    @Test
    public void calculate_ShouldReturn1For2(){
        // given
        Fibonacci fibonacci = new Fibonacci();

        // when
        long result = fibonacci.calculate(2);

        // then
        Assert.assertEquals(result, 1);
    }

    @Test
    public void calculate_ShouldReturn8For6() {
        // given
        Fibonacci fibonacci = new Fibonacci();

        // when
        long result = fibonacci.calculate(6);

        // then
        Assert.assertEquals(result, 8);
    }

    @Test(expected = Exception.class)
    public void calculate_shouldReturnExceptionForLessThan0 (){

        // given
        Fibonacci fibonacci = new Fibonacci();

        // when
        long result = fibonacci.calculate(-1);

        // then
    }


}